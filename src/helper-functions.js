export function checkProperties(itemsArray) {
  return itemsArray.every((item) => item.name && item.price && item.quantity);
}

export function numberWithCommas(x) {
  return x.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ",");
}

export class Collection extends Array {
  sum(key) {
    return this.reduce((a, b) => a + (b[key] || 0), 0);
  }
}
