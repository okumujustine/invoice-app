import axios from "axios";

export function uploadImage(file) {
  return new Promise(function(resolve, reject) {
    let reader = new FileReader();

    reader.addEventListener("load", function() {
      const fileContents = reader.result;

      const formData = new FormData();
      formData.append("upload_preset", "flt-invoice-app");
      formData.append("file", fileContents);

      let cloudinaryUploadURL = `https://api.cloudinary.com/v1_1/codefinest/upload`;

      const requestObj = {
        url: cloudinaryUploadURL,
        method: "POST",
        data: formData,
      };

      axios(requestObj)
        .then((response) => {
          resolve(response.data.secure_url);
        })
        .catch((error) => {
          reject(error.response);
        });
    });

    if (file && file.name) {
      reader.readAsDataURL(file);
    }
  });
}
