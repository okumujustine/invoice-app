import axios from "axios";
import NProgress from "nprogress";

const API_URL = "https://6011c41391905e0017be57cf.mockapi.io/sp5/api/v1";

const API = axios.create({
  baseURL: `${API_URL}`,
  timeout: 100000,
  headers: {
    "Content-Type": "application/json",
  },
});

NProgress.configure({ easing: "ease", speed: 500 });

API.interceptors.request.use((config) => {
  NProgress.start();
  return config;
});

API.interceptors.response.use((response) => {
  NProgress.done();
  return response;
});

export default API;
