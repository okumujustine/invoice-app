import API from "./axios";

export const customerService = {
  getOneCustomer(customerId) {
    return API.get(`/customer/${customerId}`).then((customer) => {
      return customer.data;
    });
  },
  getAllCustomers() {
    return API.get("/customer").then((customers) => {
      return customers.data.items;
    });
  },
  deleteCustomer(id) {
    return API.delete(`/customer/${id}`).then((customer) => {
      return customer.data;
    });
  },
  addCustomer(customerData) {
    return API.post(`/customer`, customerData).then((customer) => {
      return customer.data;
    });
  },

  editCustomer({ customerCustomerData, customerId }) {
    return API.put(`/customer/${customerId}`, customerCustomerData).then(
      (editedCustomer) => {
        return editedCustomer.data;
      }
    );
  },

  editInvoice(invoiceData) {
    return API.put(
      `/customer/${invoiceData.customerId}/invoice/${invoiceData.id}`,
      invoiceData
    ).then((editedInvoice) => {
      return editedInvoice.data;
    });
  },

  getCustomerInvoices(customerId) {
    return API.get(`/customer/${customerId}/invoice`).then(
      (customerInvoices) => {
        return customerInvoices.data;
      }
    );
  },

  getInvoiceItems({ currentCustomerId, currentInvoiceId }) {
    return API.get(
      `/customer/${currentCustomerId}/invoice/${currentInvoiceId}/invoice_item`
    ).then((invoiceItems) => {
      return invoiceItems.data;
    });
  },

  getCustomerInvoice({ currentCustomerId, currentInvoiceId }) {
    return API.get(
      `/customer/${currentCustomerId}/invoice/${currentInvoiceId}`
    ).then((invoice) => {
      return invoice.data;
    });
  },

  updateInvoiceItem({ item, currentCustomerId }) {
    return API.put(
      `/customer/${currentCustomerId}/invoice/${item.invoiceId}/invoice_item/${item.id}`,
      item
    ).then((editedInvoice) => {
      return editedInvoice.data;
    });
  },

  addInvoiceItem(customerId, invoiceId, itemToSubmit) {
    return API.post(
      `/customer/${customerId}/invoice/${invoiceId}/invoice_item`,
      itemToSubmit
    ).then((invoiceItems) => {
      return invoiceItems.data;
    });
  },

  deleteItem(itemId, currentCustomerId, currentInvoiceId) {
    return API.delete(
      `/customer/${currentCustomerId}/invoice/${currentInvoiceId}/invoice_item/${itemId}`
    ).then((deletedItem) => {
      return deletedItem.data;
    });
  },

  deleteInvoice({ id, customerId }) {
    return API.delete(`/customer/${customerId}/invoice/${id}`).then(
      (deletedInvoice) => {
        return deletedInvoice.data;
      }
    );
  },

  addCustomerInvoice(customerId, invoiceData) {
    return API.post(`/customer/${customerId}/invoice`, invoiceData).then(
      (invoice) => {
        return invoice.data;
      }
    );
  },
};
