import { customerService } from "../../services/CustomerApi";
import _ from "lodash";

const state = {
  customers: [],
  customerInvoices: [],
  customerInvoice: [{ invoice_no: null, due_date: null }],
  invoiceItems: [],
  invoiceItem: {
    id: null,
    invoiceId: null,
    createdAt: null,
    item_name: null,
    item_price: null,
    quantity: null,
  },
  customerToDelete: {
    id: null,
    name: null,
  },
  customer: {
    createdAt: null,
    id: null,
    address: null,
    avatar: null,
    name: null,
  },
  loading: false,
  addCustomerLoading: false,
  updateUserLoading: false,
  customerInvoicesLoading: false,
  customerInvoiceLoading: false,
  invoiceItemsLoading: false,
  addInvoiceItemsLoading: false,
  error: null,
  isModalVisible: false,
  isDeleteModalVisible: false,
  isEditModalVisible: false,
  isConfirmModalVisible: false,
  statusCode404: false,
  customerSucessAlert: false,
  customerAlertMsg: null,
};

const getters = {
  getCustomers: (state) => {
    return state.customers;
  },
  getCustomerToDelete: (state) => {
    return state.customerToDelete;
  },
  getCustomerToEdit: (state) => {
    return state.customer;
  },
};

const actions = {
  getAll({ commit }) {
    commit("GET_REQUEST");
    customerService
      .getAllCustomers()
      .then((customers) => commit("GET_CUSTOMERS_SUCCESS", customers.reverse()))
      .catch((error) => {
        commit("GET_CUSTOMERS_FAILED", error.message);
      });
  },

  deleteOneCustomer({ commit }, customerId) {
    customerService.deleteCustomer(customerId).then((customer) => {
      commit("DELETE_CUSTOMER_SUCCESS", customer);
      commit(
        "SHOW_SUCCESS_ALERT",
        `Customer ${customer.name} successfully deleted.`
      );
    });
  },

  addUser({ commit }, customerData) {
    commit("CUSTOMER_ADD_REQUEST");
    customerService
      .addCustomer(customerData)
      .then((customer) => {
        commit("ADD_CUSTOMER_SUCCESS", customer);
        commit(
          "SHOW_SUCCESS_ALERT",
          `Customer ${customer.name} successfully added.`
        );
      })
      .catch(() => {
        commit("CLOSE_MODAL");
      });
  },

  addInvoice({ commit }, invoiceData) {
    customerService
      .addCustomerInvoice(invoiceData.customerId, invoiceData)
      .then((invoice) => commit("ADD_INVOICE_SUCCESS", invoice));
  },

  onEditInvoice({ commit }, invoiceData) {
    customerService.editInvoice(invoiceData).then((editedInvoice) => {
      commit("INVOICE_EDIT_SUCCESS", editedInvoice);
      commit(
        "SHOW_SUCCESS_ALERT",
        `Invoice #${invoiceData.invoice_no} successfully edited.`
      );
    });
  },

  editCustomer({ commit }, customer) {
    commit("CUSTOMER_UPDATE_REQUEST");
    customerService.editCustomer(customer).then((editedCustomer) => {
      commit("CUSTOMER_EDIT_SUCCESS", editedCustomer);
      commit(
        "SHOW_SUCCESS_ALERT",
        `Customer ${customer.customerCustomerData.name} successfully edited.`
      );
    });
  },

  deleteItem({ commit }, { itemId, currentCustomerId, currentInvoiceId }) {
    customerService
      .deleteItem(itemId, currentCustomerId, currentInvoiceId)
      .then((deletedItem) => {
        commit("DELETE_ITEM_SUCCESS", deletedItem);
        commit(
          "SHOW_SUCCESS_ALERT",
          `Item ${deletedItem.item_name} successfully deleted from invoice #${currentInvoiceId}.`
        );
      });
  },

  setCustomerToEdit({ commit }, customer) {
    commit("SET_CUSTOMER_TO_EDIT", customer);
  },

  setCustomerInvoice({ commit }, invoice) {
    commit("SET_CUSTOMER_INVOICE", invoice);
  },

  setCustomerToDelete({ commit }, customer) {
    commit("SET_CUSTOMER_TO_DELETE", customer);
  },

  setItemToDelete({ commit }, item) {
    commit("SET_ITEM_TO_DELETE", item);
  },

  updateCustomerName: ({ commit }, value) => {
    commit("UPDATE_CUSTOMER_NAME", value);
  },

  updateCustomerAddress: ({ commit }, value) => {
    commit("UPDATE_CUSTOMER_ADDRESS", value);
  },

  updateInvoiceNumber: ({ commit }, value) => {
    commit("UPDATE_INVOICE_NUMBER", value);
  },

  updateInvoiceDueDate: ({ commit }, value) => {
    commit("UPDATE_INVOICE_DUE_DATE", value);
  },

  updateItemPrice: ({ commit }, value) => {
    commit("UPDATE_ITEM_PRICE", value);
  },

  updateItemQuantity: ({ commit }, value) => {
    commit("UPDATE_ITEM_QUANTITY", value);
  },

  openModal({ commit }) {
    commit("OPEN_MODAL");
  },

  closeModal({ commit }) {
    commit("CLOSE_MODAL");
  },

  openDeleteModal({ commit }) {
    commit("OPEN_DELETE_MODAL");
  },

  closeDeleteModal({ commit }) {
    commit("CLOSE_DELETE_MODAL");
  },

  openEditModal({ commit }) {
    commit("OPEN_EDIT_MODAL");
  },

  closeEditModal({ commit }) {
    commit("CLOSE_EDIT_MODAL");
  },

  getOneCustomer({ commit }, customerId) {
    customerService
      .getOneCustomer(customerId)
      .then((customer) => commit("GET_ONE_CUSTOMER_SUCCESS", customer));
  },

  getCustomerInvoices({ commit }, customerId) {
    commit("GET_CUSTOMER_INVOICES_REQUEST");
    customerService
      .getCustomerInvoices(customerId)
      .then((customerInvoices) =>
        commit("GET_CUSTOMER_INVOICES_SUCCESS", customerInvoices.reverse())
      )
      .catch((error) => {
        if (error.response.status === 404) {
          commit("SET_404_STATUS_CODE");
        }
      });
  },

  reset404({ commit }) {
    commit("RESET_404_STATUS_CODE");
  },

  getCustomerInvoice({ commit }, userInvoiceData) {
    commit("GET_CUSTOMER_INVOICE_REQUEST");
    customerService
      .getCustomerInvoice(userInvoiceData)
      .then((customerInvoice) =>
        commit("GET_CUSTOMER_INVOICE_SUCCESS", customerInvoice)
      )
      .catch((error) => {
        if (error.response.status === 404) {
          commit("SET_404_STATUS_CODE");
        }
      });
  },

  getInvoiceItems({ commit }, userInvoiceData) {
    commit("GET_INVOICE_ITEMS_REQUEST");
    customerService
      .getInvoiceItems(userInvoiceData)
      .then((invoicesItems) =>
        commit("GET_INVOICE_ITEMS_SUCCESS", invoicesItems)
      );
  },

  deleteInvoice({ commit }, invoiceData) {
    customerService.deleteInvoice(invoiceData).then((deletedInvoice) => {
      commit("DELETE_INVOICE_SUCCESS", deletedInvoice);
      commit(
        "SHOW_SUCCESS_ALERT",
        `Invoice #${invoiceData.invoice_no} successfully deleted.`
      );
    });
  },

  async submitItem({ commit }, { item, customerInvoice }) {
    commit("ADD_INVOICE_ITEM_REQUEST");
    const invoiceId = customerInvoice[0].id;
    const customerId = customerInvoice[0].customerId;

    const itemToSubmit = {
      id: Date.now(),
      invoiceId: invoiceId,
      createdAt: new Date(Date.now()).toISOString(),
      item_name: _.upperFirst(item.name),
      item_price: item.price,
      quantity: item.quantity,
    };

    customerService
      .addInvoiceItem(customerId, invoiceId, itemToSubmit)
      .then((invoicesItem) => {
        commit("ADD_INVOICE_ITEM_SUCCESS", invoicesItem);
        commit(
          "SHOW_SUCCESS_ALERT",
          `Item ${invoicesItem.item_name} successfully added to Invoice #${invoiceId}.`
        );
      });
  },

  onCloseConfirmModal({ commit }) {
    commit("CLOSE_CONFIRM_MODAL");
  },

  resetCustomerAlert({ commit }) {
    commit("RESET_CUSTOMER_ALERT");
  },

  setItemToEdit({ commit }, item) {
    commit("SET_ITEM_TO_EDIT", item);
  },

  onEditIvoiceItem({ commit }, itemData) {
    customerService.updateInvoiceItem(itemData).then((editedInvoice) => {
      commit("EDIT_INVOICE_ITEM_SUCCESS", editedInvoice);
      commit(
        "SHOW_SUCCESS_ALERT",
        `Item ${editedInvoice.item_name} successfully edited.`
      );
    });
  },
};

const mutations = {
  GET_REQUEST: (state) => {
    state.loading = true;
  },

  GET_CUSTOMERS_SUCCESS: (state, customers) => {
    state.customers = customers;
    state.loading = false;
    state.error = null;
  },

  GET_CUSTOMERS_FAILED: (state, error) => {
    state.error = error;
    state.loading = false;
  },

  DELETE_CUSTOMER_SUCCESS: (state, customer) => {
    state.customers.splice(
      state.customers.map((customer) => customer.id).indexOf(customer.id),
      1
    );
    state.isDeleteModalVisible = false;
  },

  DELETE_ITEM_SUCCESS: (state, item) => {
    state.invoiceItems.splice(
      state.invoiceItems.map((item) => item.id).indexOf(item.id),
      1
    );
    state.isDeleteModalVisible = false;
  },

  SET_CUSTOMER_TO_DELETE: (state, customer) => {
    state.customerToDelete.id = customer.id;
    state.customerToDelete.name = customer.name;
  },

  SET_ITEM_TO_DELETE: (state, item) => {
    state.invoiceItem = item;
  },

  SET_CUSTOMER_TO_EDIT: (state, customer) => {
    state.customer.id = customer.id;
    state.customer.createdAt = customer.createdAt;

    state.customer.address = customer.address;
    state.customer.name = customer.name;
    state.customer.avatar = customer.avatar;
  },

  CUSTOMER_ADD_REQUEST: (state) => {
    state.addCustomerLoading = true;
  },

  SET_CUSTOMER_INVOICE: (state, invoice) => {
    state.customerInvoice = [invoice];
  },

  ADD_INVOICE_SUCCESS: (state, invoice) => {
    state.customerInvoices.unshift(invoice);
    state.isModalVisible = false;
    state.customerInvoice = [invoice];
    state.isConfirmModalVisible = true;
  },

  ADD_CUSTOMER_SUCCESS: (state, customer) => {
    state.customers.unshift(customer);
    state.isModalVisible = false;
    state.addCustomerLoading = false;
  },

  OPEN_MODAL: (state) => {
    state.isModalVisible = true;
  },
  CLOSE_MODAL: (state) => {
    state.isModalVisible = false;
  },

  OPEN_DELETE_MODAL: (state) => {
    state.isDeleteModalVisible = true;
  },

  CLOSE_DELETE_MODAL: (state) => {
    state.isDeleteModalVisible = false;
  },

  OPEN_EDIT_MODAL: (state) => {
    state.isEditModalVisible = true;
  },

  CLOSE_EDIT_MODAL: (state) => {
    state.isEditModalVisible = false;
  },

  UPDATE_CUSTOMER_NAME: (state, value) => {
    state.customer.name = value;
  },

  UPDATE_CUSTOMER_ADDRESS: (state, value) => {
    state.customer.address = value;
  },

  UPDATE_INVOICE_NUMBER: (state, value) => {
    state.customerInvoice[0].invoice_no = value;
  },

  UPDATE_INVOICE_DUE_DATE: (state, value) => {
    state.customerInvoice[0].due_date = value;
  },

  UPDATE_ITEM_PRICE: (state, value) => {
    state.invoiceItem.item_price = value;
  },

  UPDATE_ITEM_QUANTITY: (state, value) => {
    state.invoiceItem.quantity = value;
  },

  CUSTOMER_UPDATE_REQUEST: (state) => {
    state.updateUserLoading = true;
  },

  CUSTOMER_EDIT_SUCCESS: (state, editedCustomer) => {
    state.customers = [
      editedCustomer,
      ...state.customers.filter(
        (customer) => customer.id !== editedCustomer.id
      ),
    ];
    state.isEditModalVisible = false;
  },

  INVOICE_EDIT_SUCCESS: (state, editedInvoice) => {
    state.customerInvoices = [
      editedInvoice,
      ...state.customerInvoices.filter(
        (invoice) => invoice.id !== editedInvoice.id
      ),
    ];
    state.isEditModalVisible = false;
    state.customerInvoice = [editedInvoice];
    state.isConfirmModalVisible = true;
  },

  GET_CUSTOMER_INVOICES_REQUEST: (state) => {
    state.customer = [];
    state.customerInvoicesLoading = true;
  },

  GET_CUSTOMER_INVOICES_SUCCESS: (state, customerInvoices) => {
    state.customerInvoices = customerInvoices;
    state.customerInvoicesLoading = false;
  },

  GET_CUSTOMER_INVOICE_REQUEST: (state) => {
    state.customerInvoiceLoading = true;
  },

  GET_CUSTOMER_INVOICE_SUCCESS: (state, customerInvoice) => {
    state.customerInvoice = [customerInvoice];
    state.customerInvoiceLoading = false;
  },

  GET_ONE_CUSTOMER_SUCCESS: (state, customer) => {
    state.customer = customer;
  },

  GET_INVOICE_ITEMS_REQUEST: (state) => {
    state.invoiceItemsLoading = true;
  },

  GET_INVOICE_ITEMS_SUCCESS: (state, invoicesItems) => {
    state.invoiceItemsLoading = false;
    state.invoiceItems = invoicesItems;
  },

  ADD_INVOICE_ITEM_REQUEST: (state) => {
    state.addInvoiceItemsLoading = true;
  },

  ADD_INVOICE_ITEM_SUCCESS: (state, submitedItem) => {
    state.invoiceItems.push(submitedItem);
  },

  DELETE_INVOICE_SUCCESS: (state, deletedInvoice) => {
    state.customerInvoices.splice(
      state.customerInvoices
        .map((invoice) => invoice.id)
        .indexOf(deletedInvoice.id),
      1
    );
    state.isDeleteModalVisible = false;
  },

  CLOSE_CONFIRM_MODAL: (state) => {
    state.customerInvoice = [{ invoice_no: null, due_date: null }];
    state.isConfirmModalVisible = false;
  },

  SET_404_STATUS_CODE: (state) => {
    state.statusCode404 = true;
  },

  RESET_404_STATUS_CODE: (state) => {
    state.statusCode404 = false;
  },

  SHOW_SUCCESS_ALERT: (state, msg) => {
    state.customerSucessAlert = true;
    state.customerAlertMsg = msg;
  },

  RESET_CUSTOMER_ALERT: (state) => {
    state.customerSucessAlert = false;
    state.customerAlertMsg = null;
  },

  SET_ITEM_TO_EDIT: (state, item) => {
    state.invoiceItem.id = item.id;
    state.invoiceItem.invoiceId = item.invoiceId;
    state.invoiceItem.createdAt = item.createdAt;
    state.invoiceItem.item_name = item.item_name;
    state.invoiceItem.item_price = item.item_price;
    state.invoiceItem.quantity = item.quantity;
  },

  EDIT_INVOICE_ITEM_SUCCESS: (state, item) => {
    state.invoiceItems = [
      item,
      ...state.invoiceItems.filter((invoice) => invoice.id !== item.id),
    ];
    state.isEditModalVisible = false;
    // set invoice item
    // {
    //   id: null,
    //   invoiceId: null,
    //   createdAt: null,
    //   item_name: null,
    //   item_price: null,
    //   quantity: null,
    // }
  },
};

export const customer = {
  namespaced: true,
  state,
  getters,
  actions,
  mutations,
};
