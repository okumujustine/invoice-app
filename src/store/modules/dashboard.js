const state = {
  isOpened: false,
  isImageModal: false,
  selectedImageUrl: null,
};

const getters = {};

const actions = {
  toggleNav({ commit }) {
    commit("TOGGLE_NAV");
  },

  toggleImageModal({ commit }) {
    commit("TOGGLE_IMAGE_MODAL");
  },

  setSelectedImageUrl({ commit }, url) {
    commit("SET_SELECTED_IMAGE_URL", url);
  },
};

const mutations = {
  TOGGLE_NAV: (state) => {
    state.isOpened = !state.isOpened;
  },

  TOGGLE_IMAGE_MODAL: (state) => {
    state.isImageModal = !state.isImageModal;
  },

  SET_SELECTED_IMAGE_URL: (state, url) => {
    state.selectedImageUrl = url;
  },
};

export const dashboard = {
  namespaced: true,
  state,
  getters,
  actions,
  mutations,
};
