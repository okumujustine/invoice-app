import Vue from "vue";
import moment from "vue-moment";
import FlashMessage from "@smartweb/vue-flash-message";
import JwPagination from "jw-vue-pagination";

import App from "./App.vue";
import router from "./router";
import store from "./store";
import "./assets/styles/tailwind.css";
import "nprogress/nprogress.css";
import "./assets/styles/styles.css";

Vue.config.productionTip = false;
Vue.use(moment);
Vue.use(FlashMessage, { time: 3000 });
Vue.component("jw-pagination", JwPagination);
new Vue({
  store,
  router,
  render: (h) => h(App),
}).$mount("#app");
