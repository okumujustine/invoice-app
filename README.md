# invoiceapp
- [x] Product (add, delete, update, view)
- [X] Invoice (add, delete, update, view)
- [X] Product (add, delete, update, view)
- [X] Invoice Item (add to invoice, delete from invoice, update)

## To setup the project environment locally

### Clone the project directory to your computer with cmd

```
git clone https://gitlab.com/okumujustine/invoice-app.git
```

### Switch/go to the project directory with cmd

```
cd invoice-app
```

### Install the packages required to run the app with cmd

```
npm install
```

### Then, to run the app/server, use cmd

```
npm run serve
```

### To run Tests use cmd

```
npm run test:unit
```

### Compiles and minifies for production

```
npm run build
```
