import { mount } from "@vue/test-utils";

import ConfirmModal from "@/components/ConfirmModal.vue";

describe("ConfirmModal.vue", function() {
  test("when confirm module file (ConfirmModal.vue) is instantiated", () => {
    const wrapper = mount(ConfirmModal, {
      propsData: {},
    });
    expect(wrapper.vm).toBeTruthy();
  });

  test("snapshot", () => {
    const wrapper = mount(ConfirmModal);
    expect(wrapper).toMatchSnapshot();
  });

  test("when props title exists", () => {
    const title = "flt test title";
    const wrapper = mount(ConfirmModal, {
      propsData: {
        title: title,
      },
    });
    expect(wrapper.text()).toContain(title);
  });

  test("when props closeButtonTitle exists", () => {
    const closeButtonTitle = "flt confirm button test";

    const wrapper = mount(ConfirmModal, {
      propsData: {
        closeButtonTitle: closeButtonTitle,
      },
    });
    expect(wrapper.text()).toContain(closeButtonTitle);
  });

  test("when props goToButtonTitle exists", () => {
    const goToButtonTitle = "flt go to button test";

    const wrapper = mount(ConfirmModal, {
      propsData: {
        goToButtonTitle: goToButtonTitle,
      },
    });
    expect(wrapper.text()).toContain(goToButtonTitle);
  });

  test("close button clicked emits close", () => {
    const wrapper = mount(ConfirmModal);
    wrapper.find("#close").trigger("click");
    expect(wrapper.emitted()).toHaveProperty("close");
  });

  test("go to route button clicked emits goToRoute", () => {
    const wrapper = mount(ConfirmModal);
    wrapper.find("#go-to-route").trigger("click");
    expect(wrapper.emitted()).toHaveProperty("goToRoute");
  });
});
