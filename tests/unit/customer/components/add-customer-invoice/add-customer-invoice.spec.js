import AddCustomerInvoice from "@/feature/customer/components/AddCustomerInvoice.vue";
import { mount } from "@vue/test-utils";

describe("AddCustomerInvoice.vue", function() {
  test("when AddCustomerInvoice module file (AddCustomerInvoice.vue) is instantiated", () => {
    const wrapper = mount(AddCustomerInvoice, {
      propsData: {},
    });
    expect(wrapper.vm).toBeTruthy();
  });

  test("snapshot", () => {
    const wrapper = mount(AddCustomerInvoice);
    expect(wrapper).toMatchSnapshot();
  });
});
