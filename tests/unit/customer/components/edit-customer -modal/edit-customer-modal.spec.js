import Vuex from "vuex";
import EditCustomerModal from "@/feature/customer/components/EditCustomerModal.vue";
import { mount, shallowMount, createLocalVue } from "@vue/test-utils";

const localVue = createLocalVue();
localVue.use(Vuex);

describe("EditCustomerModal.vue", function() {
  let store;
  let customer;

  beforeEach(() => {
    customer = {
      namespaced: true,
      state: {
        customer: {
          createdAt: null,
          id: null,
          address: null,
          avatar: null,
          name: null,
        },
        addCustomerLoading: false,
      },

      actions: {
        updateCustomerAddress: jest.fn(),
      },
    };
    store = new Vuex.Store({
      modules: {
        customer,
      },
    });
  });

  test("when EditCustomerModal module file (EditCustomerModal.vue) is instantiated", () => {
    const wrapper = mount(EditCustomerModal, {
      store,
      localVue,
    });
    expect(wrapper.vm).toBeTruthy();
  });

  test("snapshot of edit customer modal", () => {
    const wrapper = mount(EditCustomerModal, {
      store,
      localVue,
    });
    expect(wrapper).toMatchSnapshot();
  });

  //   it('dispatches "updateCustomerAddress" action when input event', () => {
  //     const wrapper = shallowMount(EditCustomerModal, { store, localVue })
  //     const input = wrapper.find('input')
  //     input.element.value = 'input'
  //     input.trigger('input')
  //     expect(actions.actionInput).toHaveBeenCalled()
  //   })

  test("sets the customer address input value", async () => {
    customer = {
      namespaced: true,
      state: {
        customer: {
          address: "kampala highway",
        },
      },
    };
    store = new Vuex.Store({
      modules: {
        customer,
      },
    });
    const wrapper = mount(EditCustomerModal, { store, localVue });
    const input = wrapper.find("#address");
    expect(input.element.value).toBe("kampala highway");
  });

  test("sets the customer name input value", async () => {
    customer = {
      namespaced: true,
      state: {
        customer: {
          name: "okumu justine",
        },
      },
    };
    store = new Vuex.Store({
      modules: {
        customer,
      },
    });
    const wrapper = mount(EditCustomerModal, { store, localVue });
    const input = wrapper.find("#name");
    expect(input.element.value).toBe("okumu justine");
  });

  test("when close button is clicked , it emits close", async () => {
    const wrapper = shallowMount(EditCustomerModal, { store, localVue });
    await wrapper.find("#close").trigger("click");
    expect(wrapper.emitted()).toHaveProperty("close");
  });
});
