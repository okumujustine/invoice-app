import Vuex from "vuex";
import { mount, createLocalVue } from "@vue/test-utils";

import AddCustomerModal from "@/feature/customer/components/AddCustomerModal";

const localVue = createLocalVue();
localVue.use(Vuex);

describe("AddCustomerModal.vue", function() {
  let store;
  let customer;

  beforeEach(() => {
    customer = {
      namespaced: true,
      state: {
        addCustomerLoading: false,
      },
    };
    store = new Vuex.Store({
      modules: {
        customer,
      },
    });
  });

  test("when AddCustomerModal module file (AddCustomerModal.vue) is instantiated", () => {
    const wrapper = mount(AddCustomerModal, {
      store,
      localVue,
    });

    expect(wrapper.vm).toBeTruthy();
  });

  test("snapshot add customer modal", () => {
    const wrapper = mount(AddCustomerModal, {
      store,
      localVue,
    });
    expect(wrapper).toMatchSnapshot();
  });
});
